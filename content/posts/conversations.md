---
title: "Conversations"
date: 2021-05-11T00:45:17+05:30
draft: false
tags:
  - life
---

> The conversation is the best teacher.

We get lost in those unplanned and long conversations with people yet somehow always in the end we find something new, we have learned something new.

Conversations are like those unplanned journeys, the last-minute trips which we remember for life.

Conversations make you fall in love or get out of it.

The conversation is so powerful it brings not the best but the real outside of us. If we talk enough with someone they know us better than we know ourselves.

Nowadays those conversations are getting shorter day by day. We are consuming a lot of media not listening to real people.

And it’s sad in a way that is hard to describe.

***Let’s make meaningful conversations. Let’s spend more time with the people we truly love.***
