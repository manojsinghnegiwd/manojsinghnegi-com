---
title: "When you feel unmotivated"
date: 2021-05-28T01:38:51+05:30
draft: false
tags:
  - motivation
  - life
  - success
---

*When you are unmotivated, sit down and start working.* In the best-case scenario, you will produce good work, and in the worst-case scenario, you still create something better than nothing.

We can’t wait for motivation to hit us. It’s ineffective. It’s not practical. Working even when you are unmotivated helps you make progress, and progress motivates us. 

It’s hard to start something because we haven’t made any progress, but the road gets easier as we take one step at a time as we make progress.

***Progress should never stop. If you stop working, progress will stop.***

One step at a time helps us move closer to our goal, and we can’t stop moving. We have to keep moving. It doesn’t matter how? We just have to move for now. We will improve later.

***Do you feel unmotivated? Let’s start working.***
