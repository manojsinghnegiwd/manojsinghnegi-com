---
title: "Chaos and clarity"
date: 2021-05-21T00:42:39+05:30
draft: false
tags:
  - life
  - business
---

**The world is chaotic**, and in this chaotic world, we look for clarity. We need clarity to ensure our plan gets executed precisely the way we planned and predicted, but it’s hard to find clarity in today’s world.

So much is changing every second at a scale we can’t even imagine. It’s impossible to be updated with what is happening around us. ***In other words, 100% clarity is scarce or even impossible in some cases.***

In situations like this, chasing 100% clarity is a mistake and a waste of time. We have to gather whatever information is present in front of us and execute our plan.

We have to be content with the fact that uncertainty caused by lack of clarity will be a standard part of our daily life.

***100% clarity is scarce. The world is chaotic, still we have to work with it.***
