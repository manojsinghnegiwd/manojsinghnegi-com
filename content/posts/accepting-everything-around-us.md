---
title: "Accepting everything around us"
date: 2021-05-08T00:18:23+05:30
draft: false
tags:
  - life
  - motivation
---

We live in our world. Yes, physically we all share this earth still in our mind, we have a world of our own and our understanding of how things work.

This world in our mind is drastically different from the real nature of the world. Too different.

It’s hard to accept reality or how things are because we all have ambitions, dreams, and things we want to happen, but in reality most of the time we don’t get what we want just by wanting it.

Most of the time outcome doesn’t come in our favor and we feel setback.

Even when we know most of the time we can’t control the outcome we still want things to go our way just because it hurts us to accept what is real.

Optimism. Hope. Not letting go or Not quitting are good qualities, everyone should have them, but then again accepting the truth, accepting the reality we live in, and accepting the true nature of the world is important as well.

***Make peace with the real world.*** Have a peaceful life.

