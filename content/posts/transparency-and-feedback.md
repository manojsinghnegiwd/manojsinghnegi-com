---
title: "Transparency and Feedback"
date: 2021-04-28T10:21:39+05:30
draft: false
tags:
  - transparency
  - feedback
  - software
  - team
---

We tell stories to ourselves. Our mind is designed from the very start to be curious. If we don’t know the answer or reason to something we make one. We tell a different story to ourselves or to be precise we go ahead and assume. We assume the best or the worst of the situation.

> *If we can’t improve we stay the same. If we stay the same world moves on leaving us behind.*

But these assumptions lead to a change in our perspective. We make things from thin air and then act as they were true. We think we are performing well. We think we are performing badly.

Do you want to help your fellow developers? Be brutally honest with them. Be empathetic but let them know where they are going wrong. Be open to feedback from them as well. Never back out from hard conversations. 

Most of the time we hide in our positions, meaningless meetings, and by just being busy. We hide because we are not courageous enough to stand up and say we are bad at something. 

Because that makes us a failure and failure is bad. You can’t fail. I can’t fail. Once we fail there is no coming back. We fear rejection and feedback which undermine our abilities so much we create a parallel universe where we can’t fail and anybody saying otherwise is not intelligent enough to understand our talent and skills.

We are doomed if we take this path because if we can’t fail we can’t improve. **If we can’t improve we stay the same. If we stay the same world moves on leaving us behind. Be open to feedback, know that you can fail. Know that failing is better than not being open to feedback.**

Fail. Listen to feedback. Improve. Fail better. Repeat.

