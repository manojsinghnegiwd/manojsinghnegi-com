---
title: "Let's build a culture"
date: 2021-05-29T00:01:46+05:30
draft: false
tags:
  - culture
  - life
---

Culture shapes our thinking. Culture helps us figure out what is right and wrong morally. Culture is the infrastructure upon which we build our perception of the world. The culture around us somewhat influences every social decision we take.

***Culture is what we teach our children or students. Culture is our values. [The values which derive are decisions.](https://manojsinghnegi.com/posts/whats-your-north-star/)***

That's why it makes sense to build a culture worth teaching and pass on to newer generations.

***Why don't we build a culture where helping each other out is a must?***  
***Why don't we build a culture where doing things out of courtesy is a norm?***  
***Why don't we teach our students to work together as a team instead of competing for individual positions?***  
***Why don't we teach trust and integrity?***

The world is changing too fast, and now it is up to us how we will build a culture where we all find what we are looking for in this everyday changing world.
