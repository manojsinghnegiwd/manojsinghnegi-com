---
title: "Ship - it doesn’t matter if it is perfect or not."
date: 2021-04-24T02:15:27+05:30
draft: false
tags:
  - shipping
  - motivation
---

**What is perfect?**

Perfect is different for you. Perfect is different for me. Perfect is different for your users.

Perfect doesn’t mean better. Perfect means Perfect. Which is different for you as it is for me.

You can sharpen your pencil as much as you want, giving it a perfect pointed tip. But a pencil is still a pencil. Its purpose is to write. You can write anything using a non-perfect pencil.

The same is true for software development. Your software needs to be working, not perfect. Your software needs to solve a problem and solve it well. That's it.

