---
title: "What's your north star?"
date: 2021-05-25T00:45:35+05:30
draft: false
tags:
  - motivation
  - business
---

**Money is a great motivator**, everything revolves around money mainly, but there are reasons buried deep down our consciousness which affects every step we take in our life.

***These reasons are our values.*** Our career, initiatives, products, or anything we start working on reflects these values. Whenever we start a new job or a business, we have an image in our mind of the future. What should the future look like?

Sticking with these values becomes essential because the world is unfair, the journey to build something is challenging, and it changes us. In this chaos, it's hard even to recall why we wanted to start this journey in the first place? 

In times like this, remembering those values help us. They guide us into the original directions we wanted to take. It helps us build what we want to build.

***These values are our north star.***
