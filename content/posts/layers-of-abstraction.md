---
title: "Layers of Abstraction"
date: 2021-05-17T00:42:41+05:30
draft: false
tags:
  - software
---

Abstraction in computer science is a process of only exposing the needed amount of information.

Imagine reusability. We reuse other libraries in our code without knowing what the actual logic behind the scene is.

We print some text on the screen but don’t know how and what component of the hardware is painting the pixels on the screen.

These are layers of abstraction. You don’t have to worry about how things work under the hood.

***The reason abstraction fascinates me is that it is the basis of writing or understanding reusable code.***

If you write reusable code with keeping abstraction in mind, Whenever someone is trying to solve a problem similar to yours, they don’t have to worry about solving a part that you have already solved, and they don’t have to understand your code.

The only thing they need to know is that your code solves a part of the problem, and now they can use your code, implement some of their modifications on top of it and solve a more complex problem.

Now their code can be abstracted and used by someone else to solve more complex problems and so on.

The amazing thing is with enough layers of abstractions, you can solve any complex problem without understanding every step involved in solving the problem.
