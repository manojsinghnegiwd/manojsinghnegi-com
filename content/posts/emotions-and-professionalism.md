---
title: "Emotions and Professionalism"
date: 2021-05-24T00:00:08+05:30
draft: false,
tags:
  - business
---

We are human beings because of emotions. Emotions are hard to avoid. Every moment we feel something, sadness, joy, or anger. Emotions derive our decisions. Every decision we make, either personal or professional, is influenced by how we feel.

> Emotions derive our decisions.

Yet sometimes, we sacrifice our emotions for the great good. Sometimes we make decisions logically, avoiding our emotions. Sometimes what we feel is not the right thing to do. This mostly happens in a professional environment, where we have to report bad behavior of someone we love outside work or firing someone, which will cost them their career.

When we make professional decisions like this, it’s hard to control our emotions even when we know it’s the right thing to do. We feel bad because avoiding our emotions is not something we are designed to do as human beings.

It affects us mentally, but if we ever run into a situation like this, we must acknowledge that making decisions is hard.

***It’s never going to be easy, and we have to make our peace with it.***
