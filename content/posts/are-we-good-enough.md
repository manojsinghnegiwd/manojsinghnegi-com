---
title: "Are we good enough"
date: 2021-06-22T21:16:10+05:30
draft: false
tags:
  - consistently
---

Are we good enough? We ask ourselves this often. Do we have the courage, skill-set, or discipline to achieve our goals?

We can’t logically answer this, there are some catches with the question. How much is enough? Because enough is relative to the situation. What’s the situation? Where is the end?

> Enough is infinite.

*Enough is infinite.* It’s not definite, like everything we want to measure what is enough and whether we have reached the point where we will be enough. Enough is a state of mind. The truth is we are never enough.

Once we achieve something we set out to achieve something else. We start again, learning from the start, failing and improving until we reach our goals.

The right question are 

* **What is the goal?**  
* **How am I going to achieve that?**  
* **What can I do today to move closer to the goal?**  

It doesn’t matter if we are good enough, If bit by bit everyday we are putting in the work and moving forward.

Always remember, changes are gradual. It takes time to master things and once we master something well we will have a new thing to master.

**Focus on change. Put in your best work. Consistently.**

