---
title: "What the future holds"
date: 2021-05-12T00:36:18+05:30
draft: false
tags:
  - software
---

Everyday technology is becoming easily accessible and cheap more than ever. Inevitably, a lot of our life is already digital and in the future, that’s only going to be increased.

***What does that mean for a software developer?***

Technology turf is changing rapidly day by day. If you are a software developer you already know, with so many new wearables, voice assistants, and even automated cars we can put software virtually in every area of our life.

All of our payments nowadays are digital. We buy clothes, machines, and even groceries online now. Voice assistants like Google Home & Amazon Alexa let us control all our home appliances without leaving the couch.

With that being said here is my take on what should we move on to next

**1. Voice-enabled applications**

Building voice-enabled actions for voice assistants. Imagine you have developed a voice action for a pizza shop in google home, the user just has to say “Hey Google, can you please order two pizzas from [your app]”.

The increase in voice assistants is inevitable.

**2. Virtual reality apps**

Imagine experiencing the Taj mahal in a 360 Degree view without leaving your couch or scaling the amazon forest in real-time with Bear Grylls. I mean he is Bear Grylls.

Virtual reality is somewhat here but it’s still new but its applications will be endless.

**3. Augmented reality**

The idea of merging reality and the digital world is a dream we all want to be true someday and AR ( Augmented reality ) somewhat does the job.

Pokemon go is a very popular example. Imagine you go out shopping in a shopping mall. You take out your phone and point it to a dress and your phone displays the price next to the dress. Isn’t that great?

We never know what is going to be the next big thing but trying something new doesn’t hurt? Does it?
