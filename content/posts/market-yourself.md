---
title: "Market yourself"
date: 2021-05-16T01:22:52+05:30
draft: false
tags:
  - motivation
  - business
---

Market yourself as much as you can. Your skills, your personality should be out in the wild. 

Every year thousands of people graduate and start looking for jobs. If you are one of them, how will you stand out?

The unemployment rate is sky-high, and now we are in a pandemic. How do we show what we do is so unique that organisations should hire us and not the other thousands of applicants?

***We market ourselves. We build an online personality. We share our work. We stay visible everywhere we can.***

Most importantly, we build something useful and share it with everybody.
