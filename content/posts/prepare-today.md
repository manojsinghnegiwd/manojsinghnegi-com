---
title: "Prepare today"
date: 2021-06-01T00:42:41+05:30
draft: false
tags:
  - motivation
  - life
---

Countries don’t wait to train soldiers until a war breaks out. They train soldiers before that. What’s the point of training soldiers after a war has already started? We need soldiers before a war starts.

***True.***

Then why do we wait to prepare until an opportunity comes knocking down our door?

Why don’t we start preparing today?

**We should be preparing today for what comes tomorrow, not the other way around.**
