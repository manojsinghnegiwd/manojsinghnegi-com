---
title: "Courtesy"
date: 2021-05-19T00:42:36+05:30
draft: false
tags:
  - life
---

Doing things out of courtesy is essential in every aspect of life. It can be Professional or Personal, or any other area.

Something done out of courtesy is a polite gesture or a service done without expecting anything in return. We already do those things for our family, friends, or loved ones, but we have to extend that circle further now more than ever in this pandemic which we are experiencing together.

Extending that circle will help us in creating a connected and invested community. A circle where you get help when you need it without asking for it. A circle where everybody respects each other and does things out of politeness.

An act of kindness goes a long way. It doesn’t matter professional or Personal.
