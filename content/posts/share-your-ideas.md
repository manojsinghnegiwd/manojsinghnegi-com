---
title: "Share your ideas"
date: 2021-05-30T01:52:53+05:30
draft: false
tags:
  - ideas
  - motivation
  - business
---

We should share our ideas regardless of how much of a minor impact they will make on the bigger picture.

It’s essential to recognise the contribution of different ideas & different perspectives while solving big & complex problems. When a team builds a product or a solution, **it is a collective output of all the ideas and perspectives of the team combined, not of a single person.**

In our everyday lives, we get countless opportunities to share our ideas and perspectives, which can genuinely help other people with their problems, most of the time, we choose not to share our ideas, because either we assume our ideas are not going to make much impact or we think our ideas will be rejected so what the point is?

It’s selfish to think like this. If there is the slightest possibility of helping people by sharing our ideas and perspective, we should go for it.

**Just like adding more light to a dark room helps us see clearly, adding more ideas will help us see the path clearly when we try to solve complex problems in our daily life.**
