---
title: "How to not achieve your goals"
date: 2021-06-07T01:16:56+05:30
draft: false
tags:
  - motivation
  - plan
---

In the previous post, we talked about how to make a plan to achieve your goals. Next, let’s talk about how not to achieve them.

* Make a plan and then never bother to follow it.

* Make an ambiguous plan without detailing consistent daily steps and then struggle every day to decide what to do today.

* Wait for the motivation to hit while the deadline is closing on you day by day.

* Stay busy doing random tasks while delaying the productive ones.

* Keep running from the hard conversations and never address the elephant in the room.

* Care too much about the results instead of focusing on learning from the failures.

* Do not show up to work because you are tired or don’t feel like working.

* Quit because it is too hard.

Most of the time, we fall short of achieving what we set out to achieve; instead, ***either we achieve a different goal or learn a lot in the process, and sometimes that’s better***.

**Don’t stop because you can’t achieve your goals. Instead, stop when you don’t care about them anymore, and then go find out what you care about.**

