---
title: "You Know Less"
date: 2021-05-01T00:33:25+05:30
draft: false
tags:
  - motivation
  - success
---

**We always know less.** We can’t know more about something. Everything is made of so many things. We possibly can’t comprehend all of them with our minds.

You can read every day. You can learn every day, still a lot of new things to try out, a new piece of information in every interaction.

A lot to learn. It is fascinating to even think about how much information is there.

Yet here we are, every day thinking we know better, we know how things work and we know what is the right thing to do next.

We struggle with problems every day but choose to solve them with our limited perspective, never considering the possibility of learning something new, expanding the horizon of our perspective, or changing it altogether.

We feel vulnerable. We hide behind excuses. We hide behind reasons. Never improving. Never learning.

Why is it hard to read for 30 mins every day? When we scroll through Instagram all night?

Why is it hard to go to Wikipedia and randomly learn something new? When every day we look for happiness in this self-obsessed world?

***The goal is to expand. The goal is to move. The goal is to keep learning.***
