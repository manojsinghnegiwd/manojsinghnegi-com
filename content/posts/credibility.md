---
title: "Credibility"
date: 2021-05-10T11:21:15+05:30
draft: false
tags:
  - credibility
  - life
  - shipping
  - motivation
---

> “The quality of being trusted and believed in.”

That’s how google dictionary defines credibility. In today’s world where the importance of the skillset, you can showcase is more than your qualification on paper, credibility comes into the picture.

How much does your employer trust you? Do they trust you enough with their money? Do they trust you to handle this responsibility?

It all boils down to trust. The things take years to build and can lose in seconds.

Well, we and our employers or customers don’t have years. So most people use credibility to determine your abilities to drive profits.

**Okay, but how do we improve credibility? you ask.**

Work and share as much as you can. Make your work public so if anybody comes looking to verify your work they find something.

Help out people in your network. Increase your network.

When you start talking with people in your network people start to recognise you and how many people know you in your industry is a very big factor when customers go on and research about you.

Go out there and show your work. Don’t hold back. If you are a writer. Write. If you are a software developer. Write open-source code. 

***Do anything and share. Share and make it public.***
