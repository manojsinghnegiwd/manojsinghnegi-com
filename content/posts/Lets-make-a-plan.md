---
title: "Let's make a plan"
date: 2021-06-06T00:28:10+05:30
draft: false
tags:
  - motivation
---

Let’s make a plan for your goals.

What are the things you need to do every day to achieve your goals in a given timeline?

Write them down.

Now do them every day with commitment and consistency. No matter if you feel motivated or not.

**Long-term plans only work when you work on them every day with consistency.**
