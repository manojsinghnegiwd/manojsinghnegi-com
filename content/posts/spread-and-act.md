---
title: "Spread and Act"
date: 2021-05-20T01:23:06+05:30
draft: false
tags:
  - idea
  - business
  - motivation
---

An idea in itself doesn’t hold much value. The critical factors for the success of an idea are

1. **Followers of the idea** - People who believe in that idea.
2. **Execution** - Acting on the idea instead of just talking about it.

When we have these two, success is not guaranteed, but the probability of success is likely high.

**If no one believes in an idea, it doesn’t make sense to act on it. If an idea has followers, but the execution of the idea is poor, we still fail.**

Spread your idea. It can be environmental change, product, or helping other people or companies. It doesn’t matter. Spread your idea and build a following of people ready to invest their precious time and energy into your idea.

Once you have gathered enough people, deliver your idea to them. Listen to their feedback, then deliver some more.

That’s it. That’s the cycle. ***Spread and Act***.

