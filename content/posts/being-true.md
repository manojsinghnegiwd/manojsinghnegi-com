---
title: "Being true"
date: 2021-05-15T00:00:11+05:30
draft: false
tags:
  - life
  - motivation
---

On one level or another, we lie to the world and ourselves, which is standard human behaviour.

Our brain will do everything in its power to make us feel safe, even to the point where we have to tell ourselves stories to cover up the real truth about how things are.

We know the solution. We know there are flaws. We know what areas we need to improve. Still, we hide behind this coping mechanism receding a story to ourselves where we are the victim and the whole world is just being unfair to us.

***That is a lie. The world is unfair to everybody. Everybody faces struggle in life,  challenges to overcome. You are not alone.***

That’s hard to believe, but that’s the truth.

