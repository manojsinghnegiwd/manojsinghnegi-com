---
title: "What MMA teach us"
date: 2021-05-09T00:32:00+05:30
draft: false
tags:
  - motivation
  - life
---

I love watching MMA. It’s a high-speed action sport and pretty exciting too. The outcome changes in a second sometimes. The opponent who is winning this very moment can be on the losing side in no time.

The core rule of MMA fighting is simply to beat your opponent until they give up or are knocked down.

As much as it is exciting for us viewers, it’s really scary for the people who are participating in it. Imagine you are in a locked cage with someone who will not stop beating you until you are on the ground.

Watching them bleed or brutally hitting each other is not the reason I love MMA. **It’s exactly the opposite. The calmness. The wait. The mindset and thought process to stay calm and not give in to the frustration. It’s unbelievable.**

Even in that high-speed drama situation, they stand their ground. They take punches. They analyze the opponent waiting for their opportunity to turn the game around. They don’t give in to the frustration and start throwing punches. **The minute someone loses their control and starts hitting without planning that person is lost.** If you don’t have control over your hands you will not be able to block the next knock-down punch. It’s the way of MMA.

What does that teach us?
======

When we let frustration or anger get the best of us, we forget what we know. We make decisions without thinking only to regret them afterward.

MMA teaches us to keep calm in scary situations. It can be anything in your life but you have to keep calm and keep fighting. You have to stand your ground and look for your opportunity. You have to go back and focus on what you know and how calmly you can engage with the situations.

***Don’t let the frustration or anger get the best of you. You know better.***
