---
title: "Getting exhausted"
date: 2021-06-23T01:15:37+05:30
draft: false
tags:
  - motivation
  - relax
---

Working hard and working consistently results in exhaustion. It’s important to relax once in a while and most importantly if you are exhausted. Have one day in a week for yourself, for your family and your friends. 

Try gardening, or mediation, maybe going out will work. It’s important to have emotional connections with people you love. These connections provide us with the moral support we need to survive in stressful situations.

Doing what you love doesn’t mean you’ll always feel great, doing your best work takes unimaginable effort every day.

***Let’s relax. Put your laptop down. Go out and have fun for once.***

> *I know the people who know me personally will be like, He doesn't go out and have fun. He programs for like 12 hours a day including weekends. Well, truth to be told, programming is **fun for me.*** ;)

**Find what helps you relax and do it often after putting in a lot of hard work.**
