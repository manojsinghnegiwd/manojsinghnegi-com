---
title: "Perspective"
date: 2021-05-14T00:52:06+05:30
draft: false
tags:
  - life
  - motivation
---

Human beings have a huge misconception about how others feel or think in a given situation. Most of the time, we assume everyone feels or thinks like us and who doesn't are somehow different and disagree with us.

Because of this behaviour, most of the time, we make poor decisions. We don't understand the other person's motivation because that can never happen for us, and if It can't happen for us, how come the other person is feeling this way.

This kind of behaviour is rooted in us and our society as well. If someone has a different opinion than us, it's hard for us to have a conversation with that person, and I think that's a bad thing.

> To gain knowledge, we have to let go of what we know and try different perspectives and opinions.

When we have disagreements or different perspectives, we receive new knowledge. Maybe we were wrong, and I think that's the real reason behind why we don't want to talk to someone with a different opinion, because we fear we might be wrong and if we were wrong about one thing, we could be wrong about other things as well, and that's scary.

Imagine whatever you believed till now was a hoax. That's no less than an identity crisis.

***Still, dropping our perspective and talking with people, understanding them and their point of view, really will help us grow.***
