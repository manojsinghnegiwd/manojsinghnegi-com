---
title: "There is no such thing as bad code"
date: 2021-04-27T09:36:23+05:30
draft: false
tags:
  - software
  - code quality
---

Either good code or the code which needs your attention to be improved.

There is countless standards of coding. Different people have different styles and it’s impossible to meet all of the standards.

Focus on making your applications good. Everything else comes later. We don’t get paid to code. We get paid to make this world a better place by writing good software.

***Writing code which helps us accomplish good software.***  
***That code is of the greatest quality.***

Writing better code is improving on the previous iteration. We don’t get everything right on the first try. 

My process is, write code. Test it. It works voila!!. Refactor it. Repeat.
