---
title: "Being Average"
date: 2021-05-31T00:38:11+05:30
draft: false
tags:
  - business
  - motivation
---

Jobs are well defined, or at least we want jobs with a well-defined description. We want to measure our efforts. How much effort will it take? 

We need a task that can be completed, and the job is done. Great work. That’s what everybody does. Everybody completes their tasks, and because everybody does it, it’s average. 

We can complete our job successfully, well the truth is most of the people can do that. That’s not a new thing. That’s not the reason why someone should hire us or buy from us.

Why don’t we go beyond that? Why don’t we complete our job and then start working on making things better or faster? Why stop after completing the job?

**Completing the job is the bare minimum. It’s the lowest bar. When we go beyond and make things better, that’s where we innovate. The quest is to make things better, not just make them and stop.**

