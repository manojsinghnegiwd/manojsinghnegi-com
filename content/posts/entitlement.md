---
title: "Entitlement"
date: 2021-05-26T00:51:45+05:30
draft: false
tags:
  - life
  - motivation
---

***The belief that one is inherently deserving of privileges or special treatment.***

This is how Google describes the feeling of entitlement. The belief that for some reason, we deserve more without doing much or without proving that we deserve what we desire.

We feel entitled to job security without actually making profits for our employer.  
We feel entitled to respect without respecting others.  
We feel entitled to success without putting in the hard work. 

If we are not willing to work for something we desire, why do we feel that we deserve it?

***Before blaming how unfair this world is for not giving what we truly deserve, we should ask ourselves, why?***

Why will someone hire us?  
Why will people respect us?
Why will people reward us?

***We should put in our best efforts and work hard before feeling entitled.***
