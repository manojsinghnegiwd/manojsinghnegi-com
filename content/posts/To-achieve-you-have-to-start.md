---
title: "To Achieve You Have to Start"
date: 2021-05-27T19:25:26+05:30
draft: false
tags:
  - life
  - motivation
---

***Start soon. Start today. Start now.***

You don’t have to worry about being perfect, and you don’t have to worry about reaching the destination. Put that aside for a few moments.

You have to start. You have to take the first step. Make improvements over time. Learn and implement.

You tell yourself you still need to learn or are not talented enough. Once you have enough skills, you are going to start. It’s an excuse. You are stalling.

***To achieve, you have to start.***
