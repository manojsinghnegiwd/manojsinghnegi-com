---
title: "Gift wrapper"
date: 2021-06-04T00:41:15+05:30
draft: false
tags:
  - business
  - motivation
---

Why do we wrap gifts? When only the gift matters, why invest our energy into wrapping them?

The most obvious explanation is we want to go the extra mile. We want to show we care about the receiver so much even the little things matter.

The wrapper doesn’t matter. Intention does. It’s not about the act itself. It’s the intention that moves us, which makes us emotional.

It’s about how we make someone feel, and that’s all that matters.
