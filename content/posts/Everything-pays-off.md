---
title: "Everything pays off"
date: 2021-05-03T00:30:39+05:30
draft: false
tags:
  - motication
  - misc
---

> Eventually, everything pays off.

The song you have written. The movies you have watched. Every day with our every interaction we learn something. The internship you did after college or those experiments you did with your new business pays off.

Failure pays off. Does nobody like what you do? It’s a chance of improvement and that’s how failure pays off.

Every skill. Every extra class. Every curriculum activity. Your every experience in every area of life eventually helps you do something else better. Everything is connected somehow to something.

*You did something good for someone. Someone else will do good for you.*

It’s about doing and being content with it, without expecting and without attachment to the result.

Everything eventually pays off. You are not wasting your time learning something new for no reason. ***It’s knowledge, and like everything, it pays off.***

