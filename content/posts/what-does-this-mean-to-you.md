---
title: "What does this mean to you"
date: 2021-06-08T02:23:15+05:30
draft: false
tags:
  - perspective
---

One thing can mean very different things for different people with different perspectives. I think programmers should be marketers; they should write about their work and help other software developers out.

Most of the programmers will disagree with me. For some people, it will mean just coding or algorithms or just designing websites.

***We have one perspective. Our perspective. A perspective that we grew up with. We never can experience the same things the same way a different person experiences them. That’s not possible.***

Still, every day when we talk with each other, we assume that people feel the same as us about topics we are discussing. When we market our products, we believe everybody tells the same story to themselves, which we told ourselves earlier.

When we are helping out someone in a bad situation, we keep asking ourselves why they are in this bad situation in the first place? No one in their right mind can go this wrong.

We all have a narrative, a personal one. We faced a lot of things in our lives, individually. We told ourselves a lot of stories. We watched a lot of stories, and all of this shapes our perspective.

**Some things will make sense some will not, but even if they don’t make sense to you, they will to someone else.**
