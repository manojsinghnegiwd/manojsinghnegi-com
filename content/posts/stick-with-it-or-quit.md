---
title: "Stick with it or quit"
date: 2021-05-23T01:57:48+05:30
draft: false
tags:
  - motivation
---

Find what you want to do. Go out of your comfort zone and meet new people, try new things. Once you have something you care about, do it. Do it every day. Stick with it even when you are on the verge of failure, even when there is very little chance of success. If you care about what you do, stick with it.

***Time is powerful. With enough time on your hands and enough hard work, unimaginable things can be achieved, and sometimes it takes years to build something worthwhile.***

When things get messy and nothing seems to work, that’s the exact situation where breakthroughs happen. When you are the most uncomfortable, that is when you are learning something new. If you let these moments take the best out of you, that is when you truly fail.

> If you don’t care about what you do, quit right now. Find something interesting.

Don’t quit in these situations. If you want to quit, either quit at the start or when you no longer care. If you don’t care about what you do, quit right now. Find something interesting.

With that being said, It’s important to be realistic with ourselves. If something is truly not working, two things can be done, either find ways to make it work or quit even if you care about it. Go and find something else to work on.

