---
title: "Instant gratification"
date: 2021-06-21T19:01:45+05:30
draft: false
tags: 
  - goals
  - motivation
---

We live in the world of instant gratification, want to buy something? In a few taps, it's done. Are you single, let's install tinder. Everything is available in an instant.

Instant gratification is a habit, the point where this habit becomes a problem for us is when the process of getting something is about keeping patience when there is no timeline on when we are going to achieve the things we desire.

Sometimes we have to work years towards a goal to achieve it, everyday we have to put our small efforts with consistency. In times like this, we can't have instant gratification and that's one of the biggest reasons why people quit.

We start working towards our goal. We are motivated. We put in our efforts. One day. Second day. Third day, on the fourth we look at our goals and feel they are too far away because we can't achieve them instantly. We throw away our hopes and quit.

***Sometimes we have to trust the process, keep patience and work towards our goals consistently without looking for instant results.***
