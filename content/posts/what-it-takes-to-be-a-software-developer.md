---
title: "What it takes to be a software developer"
date: 2021-05-05T09:14:38+05:30
draft: false
tags:
  - software
  - developer
---

Endless nights.

A Hell lot of code each and every day.

It takes persistence, professionalism, willingness to learn and change your perspective on a daily basis.

It takes kindness and respect towards others. It takes the willingness to keep coming back when failure knocks you down.

It takes the mindset to keep looking for an answer when it seems impossible to find an answer.

It takes the ability to break down complex problems into simpler ones.

It takes the ability to convert coffee/tea into code.

It takes the ability to understand people and their requirements alongside thousands of different business models.

It takes management skills. It takes communication skills both verbal and written.

It takes blurring the line between work and fun. 

But do you know what it takes to be the best developer you can ever be?

The courage to find a bug on friday evening and report it instead of hiding it so you don’t have to fix it staying all night long.

