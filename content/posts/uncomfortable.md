---
title: "Uncomfortable"
date: 2021-05-06T00:26:37+05:30
draft: false
tags:
  - motivation
  - success
---

When we try out something new the feeling we get is Uncomfortable. When we jump out of our comfort zone and say I want to do that we feel uncomfortable and it doesn’t feel too good.

We want to feel safe and feeling uncomfortable is far away from that. We resist it. We fight it. We go all weird and start hiding.

We start retreating. 

> The secret is being comfortable with being uncomfortable.

But before solving anything, learning something new, or before trying out something you always wanted, you feel uncomfortable. 

All of these things are good for you. So is feeling uncomfortable good or bad?

It’s bad if every time you feel uncomfortable you hide.

It’s good if you learn to be comfortable with being uncomfortable.

Most of us don’t go outside the box because we fear crossing walls and boundaries. We fear to go out into uncharted territories without maps.

And if you can overcome that fear. You can do wonders.

***The secret is being comfortable with being uncomfortable.***