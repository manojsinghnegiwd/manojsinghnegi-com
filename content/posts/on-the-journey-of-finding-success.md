---
title: "On the Journey of Finding Success"
date: 2021-04-25T12:20:44+05:30
draft: false
tags:
  - success
  - motivation
---

We strongly want to believe there is a secret recipe to success. To achieve our goals. We look for it everywhere. In people. In books. On the internet. There are lot of people who can give you those recipes but most of the time they don't work.

Because we forget the most important thing. They tell you what worked for them. Their audience. Their customers. Their dreams. Not yours.

You have to be flexible. They give you a guideline to work with. How they overcame their fear. How they achieved their dreams.

Now you have to find yours and there is no single, pre-made recipe for it. You have to get out there. Show up every day. Work when you don’t feel like it. Stay consistent. Write every day. Code every day. Build every day. Get feedback from your audience and repeat.
