FROM  klakegg/hugo:0.82.0-alpine as build
WORKDIR /app
COPY . /app
RUN hugo -D

FROM nginx:1.17.8-alpine
COPY --from=build /app/public /usr/share/nginx/html
COPY --from=build /app/blog /usr/share/nginx/html/blog
COPY --from=build /app/Manoj_Singh_Negi_CV.pdf /usr/share/nginx/html
RUN rm /etc/nginx/conf.d/default.conf
COPY ./nginx.conf /etc/nginx/conf.d
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
