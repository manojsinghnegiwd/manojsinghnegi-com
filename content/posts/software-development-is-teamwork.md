---
title: "Software development is teamwork"
date: 2021-05-04T09:19:42+05:30
draft: false
tags:
  - software
---

**Software development is teamwork.** If you think you are the only person doing all the work. Think again. No one can do it alone. We need help and we get it either directly or indirectly.
You may have solved this problem but not without the help of others. So let’s respect that.

Someone has pitched to get the project.

Someone has found the bug in your project before it goes live.

Someone has helped you out when you were stuck with a stupid bug for the last 2 days.

Someone has given a different perspective to a problem.

Software development is teamwork if you think you do it all alone.

Knock knock

You are wrong.
