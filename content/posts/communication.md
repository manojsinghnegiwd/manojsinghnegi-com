---
title: "Communication"
date: 2021-05-18T13:37:25+05:30
draft: false
tags:
  - communication
---

Communication can refer to several things. In simpler terms, it means exchanging information through some medium. The information can be verbal, written, or hidden in your body language.

The fascinating thing about communication is that its invention helped us shape the world as we know it today because we can communicate with each other, we can collaborate, negotiate or disagree with each other.

Communicating with each other brings us together. It enables us to spread ideas or listen to stories, shaping our perspective and increasing our knowledge.

Even after so many years of communicating, we still lack in communication.

Most of the problems we face when we are engaging with other people are miscommunication. It can be your workplace, your family at home, or friends. Most of the fights and disagreements are signs of miscommunication.

With a little bit of effort and transparency while handling situations like this solves the problem most of the time, and that’s what we have to do more.

We have to be more transparent with other people. We have to be more patient and listen to their opinion as well.

Translate your ideas into plain language and listen to every single word someone says talking to you. It solves most of your problems.

