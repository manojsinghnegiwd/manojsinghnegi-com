---
title: "Better sooner than later"
date: 2021-04-29T09:03:06+05:30
draft: false
tags:
  - software
  - shipping
---

***Catch that bug early. Better sooner than later.***

Catching them while developing might be a good place, or when you just deployed the code. How about giving that new feature a last testing round?

Most of the time, Most of us ( the software developers ). Don’t test our code. Sometimes because of overconfidence or sometimes we are just tired.

Sometimes we ignore it. If we found a hole in the feature, we have to stay up late and fix that before leaving.

And sometimes it’s just Friday.

But ignoring it doesn’t solve the problem. It is going to come back. You know that. 

When someone makes the payment in our app. It’s going to come back. When someone is doing the most important thing that software is designed to do. You know it. It’s going to come back.

**When that bug will come into production. We are doomed. We lose important users, the users who gave a chance to our app after months of development and marketing. In seconds those users choose to go away.**

*Stop ignoring those bugs. Fix them. Test them. Fix some more. Ship the application. Repeat.*
