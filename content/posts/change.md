---
title: "Change"
date: 2021-06-10T02:02:07+05:30
draft: false
tags:
  - work
  - motivation
---

Good work brings change. A change in how a customer uses an application. A change in how much profit your client makes. A change in society. A change in your followers. A change in perspective.

When we do our best work, somebody gets affected. Someone gets the benefit and someone gets inspired.

***The work that inspires others changes others and makes people profit, that's the type of work we need to do.***

