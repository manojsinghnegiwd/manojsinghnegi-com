---
title: "Vaccination"
date: 2021-06-03T00:16:21+05:30
draft: false
tags:
  - prepare
  - motivation
---

I got vaccinated today. The way vaccination works are simple. We provide our body with a tiny part of a pathogen ( an organism that causes disease ), which is small enough to not cause any harm to us and big enough to trigger our immune system to build antibodies so the next time we catch the disease, we can fight it on our own.

That’s interesting. We have a complex problem, so instead of solving it all together, we expose ourselves to a tiny part of that problem, which will help us to understand and build solutions for the next time when the problem presents itself again.

We don’t have to solve the problem right now, at least not all of it. What we can do is prepare ourselves instead of waiting for the opportunity and then acting surprised because we don't know what do.

***When you can predict, predict and prepare.***
