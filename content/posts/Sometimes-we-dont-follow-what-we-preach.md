---
title: "Sometimes we don't follow what we preach"
date: 2021-06-05T00:27:17+05:30
draft: false
tags:
  - motivation
---

Sometimes we don't follow what we preach.

Maybe we have learnt something new or maybe we had to improvise. Sometimes we are in a situation where we can’t follow the same set of rules we ask others and ourselves to follow.

We learn. We adapt. We improve. These scenarios change how we perceive our surroundings and in some situations even to change our beliefs.

Sometimes we don’t have the willpower to keep our words and that makes us human.

***It’s important to show up rather than being right all the time.***

