---
title: "Salary vs Salary Capacity"
date: 2021-05-22T16:35:59+05:30
draft: false
tags:
 - business
---

In recent years, competition in the job market has become fierce, and people with exceptional qualities are preferred across the globe. The market is transformed to only reward people who stand out and make a difference for their organisations, who bring something unique to the table.

In this growing area, we have to ensure that we constantly increase our salary capacity. Let me explain. Salary is our current value to an organisation. Salary capacity is what our value can be to an organisation.

***Salary is a safety net.*** We will receive our salary no matter what, even if our performance goes down or we continue to make the same impact as last year, whereas Salary capacity is directly linked to the increasing impact initiated by us, which brings a positive outcome for the company.

Increasing your salary capacity is getting critical day by day because if we don’t, we will not perform as good as we think in this new job market. To improve our salary capacity, we have to improve our skillset, strategies, or knowledge to impact our organisation positively.

***That’s the future. It’s important to understand for both organisations and employees that providing a positive outcome for the company is beneficial for everybody.***
