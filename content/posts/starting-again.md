---
title: "Starting again"
date: 2021-05-07T00:26:28+05:30
draft: false
tags:
  - motivation
  - success
  - knowledge
---

We shed 30,000 - 40,000 dead skin cells every minute of the day. Yes, every minute. Our body gets rid of these old cells and creates new ones.

Even snakes have to shed their skin because their body outgrows their skin and now they can’t fit into it anymore.

Our knowledge sometimes outgrows our perception and to let it flourish, we have to let go of our perspective just like the skin and look at things differently.

We have to start new, with an empty glass.

We have to accept the gift of knowledge by giving up what we think we know.

***Sometimes the only way to know more is to know less again.***

