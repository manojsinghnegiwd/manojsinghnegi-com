---
title: "Commitment or Motivation"
date: 2021-05-13T00:00:43+05:30
draft: false
tags:
  - motivation
  - commitment
  - shipping
  - life
---

Motivation is best described as when someone feels they can achieve anything, nothing can stop them, and no matter how many times they fail, they eventually succeed.

One other attribute of motivation is it is not here to stay. Motivation comes and goes. One day you will feel motivated the next day not so much. In one moment, you feel you can conquer the world. The next moment can't even do one simple thing.

Someone can demotivate you as well. Also, there is no method to make someone feel motivated. It's just a feeling. Most of the time, we can't control it.

Still, every day we look for motivation, and if we don't feel motivated, we just quit for that day.

Let's take commitment on the other hand. Commitment is discipline. No matter what the situation is, if you commit to something, you have to do it.

**Do you want to be a writer? Make a commitment to yourself about writing a paragraph every day.**

**Do you want to be a singer? Make singing a song every day a commitment.**

**Do you want to be a software developer? Well, make writing 100 lines of code outside work a commitment.**

**And stick to it, not feeling motivated? No problem, you have a commitment to keep.**

It's better to be committed rather than be motivated. Being motivated is good, but remember we feel good and motivated when every day we work hard. We don't work because we feel motivated.

***Commit and stick to it. It doesn't matter if you feel motivated or not.***

