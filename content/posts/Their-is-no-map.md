---
title: "There is no map"
date: 2021-06-02T00:35:31+05:30
draft: false
tags:
  - business
  - work
  - motivation
---

There is no map.

It's all trial and error.

There is no easy way.

It's all hard work.

It's about delivering again and again.

It's about listening to feedback and delivering again.

It's about making things and then making them better.

It's a long journey with small but consistent steps.

It's about committing to do work without waiting for motivation.

It's about rising above boring work and making something new.

It's about helping others reach their dreams, goals, and needs so that they can help us achieve our goals in return.

