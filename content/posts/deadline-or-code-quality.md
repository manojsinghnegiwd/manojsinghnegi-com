---
title: "Deadline or code quality"
date: 2021-05-02T11:07:44+05:30
draft: false,
tags:
  - software
  - motivation
  - code quality
  - shipping
  - deadlines
---

Why not both?

Reality matters, even if it is not fair. We can fight it as much as we want, but if we don’t work for real people, real organizations, and real customers, our work is not real.

Working for real people will bring real deadlines. 

Why do we even have deadlines?

Because time and money are on the stack and we live in the real world. We don’t get paid to write code, we get paid to make useful and profitable software.

Profit can be money, happiness, or just fun for someone, whatever it is we have to make our product profitable in a period of time or our client loses money and time.

In the journey of writing the best software possible writing good code which is

* **Easy to understand**
* **Better handle edge cases**
* **Easily configurable**

Helps us deliver application quickly instead of undermining our ability to deliver on time.

***It’s a myth and an excuse.***

> We can learn to be fast. We can learn to be good. We can learn to be fast and good.

There is no stopping it. It’s a stereotype. We cling to it as our only lifeline to avoid improving our code quality and delivery time, because improving is hard and we need a shield from all the unfair things which can go wrong. 

Constraining ourselves to do the job in a given timeline is part of our job, still thousands of developers around the world waste a lot of money and time giving this excuse to every client they meet.

***If we don’t push ourselves, how are we supposed to grow?***

If you have to fit clothes inside the only suitcase you got and your flight is going to leave in 30 minutes, believe me, you will do anything in your power to fit those clothes inside the suitcase.

Because if you don't you will lose your money and your time as well.

***Why don't we implement the same analogy with deadlines?***