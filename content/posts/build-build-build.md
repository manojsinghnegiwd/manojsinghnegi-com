---
title: "Build. Build. Build."
date: 2021-04-26T09:50:33+05:30
draft: false
tags:
  - building
  - motivation
---

**Just build. Build anything. Build everything. Build clones. Build prototypes.** Build just for the sake of building. Building things makes you an engineer. Someone who can build. Someone who knows what it takes to build in the face of adversity.

*the more you sweat in practice the less you bleed in battle - **Norman Schwarzkopf***

It’s true. It is going to cost you a little if you build and fail now than when you build for someone else.

It’s like practicing math problems. You go on and solve problems until you ace the exams.

The only difference is we don’t stop once we build one product. We keep going, we keep building.

We keep improving.
