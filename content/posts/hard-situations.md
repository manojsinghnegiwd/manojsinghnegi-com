---
title: "Hard situations"
date: 2021-06-09T19:48:48+05:30
draft: false
tags:
  - hard
---

Everyone performs well in a normal situation. That is why it’s normal. However, the real test of our skills starts when we encounter a hard situation.

A hard situation can take many forms. 

* Quick deadlines
* Hard customers
* Business not doing well
* Selling a new product to a new audience
* And many more

How we act in hard situations is the real test. A situation that is different from any situation we have ever encountered before. A situation that makes us uncomfortable. A situation where we are responsible for profit & loss.

***That’s scary, and that’s the test because if it were easy, anyone would have performed well in those situations, and if everyone can do that, then we are not as special as we think we are.***

We run away from hard conversations and hard situations. We run away from responsibilities and blame, and if we are running away, maybe we don’t care about the things we are trying to achieve. In that case, let’s quit and find something we want to do.

We want to be at the top of what we do.  
We want to be appreciated for our work.

**Then let’s start making a difference. Let’s start making our efforts count. Let’s start putting ourselves in hard situations and try to give our best. Let’s start taking responsibility for results.**

